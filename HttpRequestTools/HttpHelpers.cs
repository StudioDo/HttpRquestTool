﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;

namespace HttpRequestTools
{
    class HttpHelpers
    {
        /*public static byte[] BuildMultipartPostData(string Boundary, Dictionary<string, string> HttpPostHeader, Dictionary<string, string> HttpPostBody, PostFileData FileUploadData)
        {
            StringBuilder sb = new StringBuilder();
            // append access token.
            //sb.AppendLine("--" + Boundary);
            //sb.AppendLine("Content-Disposition: form-data;name=\"access_token\"");
            //sb.Append(Environment.NewLine);
            //sb.AppendLine(FBGraphAPIConfiguration.FBValidAccessToken);
            // append form part.

            if (HttpPostHeader != null && HttpPostHeader.Count > 0)
            {
                foreach (KeyValuePair<string, string> HttpPostDataItem in HttpPostHeader)
                {
                    sb.AppendLine("--" + Boundary);
                    sb.AppendLine(string.Format("Content-Disposition: form-data;name=\"{0}\"", HttpPostDataItem.Key));
                    sb.Append(Environment.NewLine);
                    sb.AppendLine(HttpPostDataItem.Value);
                }
            }


            if (HttpPostBody != null && HttpPostBody.Count > 0)
            {
                foreach (KeyValuePair<string, string> HttpPostDataItem in HttpPostBody)
                {
                    sb.AppendLine("--" + Boundary);
                    sb.AppendLine(string.Format("Content-Disposition: form-data;name=\"{0}\"", HttpPostDataItem.Key));
                    sb.Append(Environment.NewLine);
                    sb.AppendLine(HttpPostDataItem.Value);
                }
            }
            // handle file upload.
            if (FileUploadData != null)
            {
                sb.AppendLine("--" + Boundary);
                sb.AppendLine(string.Format("Content-Disposition: form-data;name=\"{0}\"; filename=\"{1}\"", FileUploadData.Name, FileUploadData.FileName));
                sb.AppendLine(string.Format("Content-Type: {0}", FileUploadData.ContentType));
                sb.Append(Environment.NewLine);
            }
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms);
            bw.Write(Encoding.UTF8.GetBytes(sb.ToString()));
            bw.Write(FileUploadData.FileData);
            bw.Write(Encoding.UTF8.GetBytes(Environment.NewLine));
            bw.Write(Encoding.UTF8.GetBytes("--" + Boundary));
            ms.Flush();
            ms.Position = 0;
            byte[] result = ms.ToArray();
            bw.Close();
            return result;
        }
        */
        //而實際傳送的程式就無需特別的修改，但唯一要注意的是要加上 boundary 參數，否則多訊息部份的能力會無法使用。
        /*public static string MakeHttpPost(string RequestUrl, Dictionary<string, string> HttpPostData, PostFileData FileUploadData)
        {
            HttpWebRequest request = WebRequest.Create(RequestUrl) as HttpWebRequest;
            HttpWebResponse response = null;
            StreamReader sr = null;
            string boundary = "===============" + DateTime.Now.Ticks.ToString();
            try
            {
                request.Method = "POST";
                request.ContentType = "multipart/form-data; boundary=" + boundary;
                byte[] multipartPostData = HttpHelpers.BuildMultipartPostData(boundary, HttpPostData, FileUploadData);
                BinaryWriter bw = new BinaryWriter(request.GetRequestStream());
                bw.Write(multipartPostData);
                bw.Close();
                response = request.GetResponse() as HttpWebResponse;
                sr = new StreamReader(response.GetResponseStream());
                string responseData = sr.ReadToEnd();
                sr.Close();
                response.Close();
                return responseData;
            }
            catch (Exception)
            {
                throw;
            }
        }*/
    }
}
